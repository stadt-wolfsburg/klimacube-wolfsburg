#include <DHT.h>
#include "SdsDustSensor.h"
#include <MKRWAN.h>
#include "ArduinoLowPower.h"

#define DHTPIN 2
#define DHTTYPE DHT21
#define SOUND_SENSOR_PIN A3
#define VREF 5.0
#define ANALOG_IN_PIN A1

#define RESISTOR_R1 30000.0
#define RESISTOR_R2 7500.0
#define REF_VOLTAGE 3.3

#define SLEEP_DURATION 300000 // 5 minutes in milliseconds
#define LORA_BAND EU868

DHT dht(DHTPIN, DHTTYPE);
SdsDustSensor sds(Serial1);
LoRaModem modem;

#include "arduino_secrets.h"
String appEui = SECRET_APP_EUI;
String appKey = SECRET_APP_KEY;

void adjustTemperature(float &temperature) {
  if (temperature >= 15 && temperature < 20) {
    temperature -= 2;
  } else if (temperature >= 20) {
    temperature -= 5;
  }
  temperature = constrain(temperature, -40.0, 80.0);
}

void adjustSound(float &sound) {
  sound += 15;
  sound = constrain(sound, 30.0, 130.0);
}

void adjustHumidity(float &humidity) {
  humidity += 10;
  humidity = constrain(humidity, 0.0, 100.0);
}

void setup() {
  dht.begin();
  sds.begin();

  if (!modem.begin(LORA_BAND)) {
    while (1); // Stay here if modem initialization fails
  }

  if (!modem.joinOTAA(appEui, appKey)) {
    while (1); // Stay here if connection fails
  }
}

void loop() {
  // Read and adjust sound level
  float voltageValue = analogRead(SOUND_SENSOR_PIN) / 1550.0 * VREF;
  float dbValue = voltageValue * 50.0;
  adjustSound(dbValue);

  // Read and adjust humidity
  float humidityValue = dht.readHumidity();
  if (isnan(humidityValue)) {
    return;
  }
  adjustHumidity(humidityValue);

  // Read and adjust temperature
  float temperatureValue = dht.readTemperature();
  if (isnan(temperatureValue)) {
    return;
  }
  adjustTemperature(temperatureValue);

  // Read and calculate input voltage
  int adcValue = analogRead(ANALOG_IN_PIN);
  float adcVoltage = (adcValue * REF_VOLTAGE) / 1024.0;
  float inVoltage = adcVoltage * (RESISTOR_R1 + RESISTOR_R2) / RESISTOR_R2;

  // Read PM values
  sds.wakeup();
  delay(2000);

  PmResult pm = sds.queryPm();
  if (pm.isOk()) {
    float pm25Value = constrain(pm.pm25, 0.0, 999.0);
    float pm10Value = constrain(pm.pm10, 0.0, 999.0);

    // Send data via LoRa
    modem.beginPacket();
    modem.print(dbValue, 0);
    modem.print(",");
    modem.print(humidityValue, 0);
    modem.print(",");
    modem.print(temperatureValue, 1);
    modem.print(",");
    modem.print(inVoltage, 2);
    modem.print(",");
    modem.print(pm25Value, 1);
    modem.print(",");
    modem.print(pm10Value, 1);
    modem.endPacket(true);
  }

  sds.sleep(); // Put SDS011 sensor to sleep

  // Trigger a sleep for 5 minutes
  LowPower.sleep(SLEEP_DURATION);
}

