#include <SPI.h>
#include <DHT.h>
#include "SdsDustSensor.h"
#include <MKRWAN.h>
#include "ArduinoLowPower.h"

#define DHTPIN 2
#define DHTTYPE DHT21
DHT dht(DHTPIN, DHTTYPE);

#define SoundSensorPin A3
#define VREF 5.0

#define ANALOG_IN_PIN A1
float adc_voltage = 0.0;
float in_voltage = 0.0;
float R1 = 30000.0;
float R2 = 7500.0;
float ref_voltage = 3.3;
int adc_value = 0;

SdsDustSensor sds(Serial1);

LoRaModem modem;

#include "arduino_secrets.h"
String appEui = SECRET_APP_EUI;
String appKey = SECRET_APP_KEY;

void adjustTemperature(float &temperature) {
  if (temperature >= 15 && temperature < 20) {
    temperature -= 2;
  } else if (temperature >= 20) {
    temperature -= 5;
  }
}

void adjustSound(float &sound) {
  sound += 15;
}

void adjustHumidity(float &humidity) {
  humidity += 10;
}

void setup() {


  dht.begin();
  sds.begin();

  if (!modem.begin(EU868)) {
    Serial.println("Failed to start module");
    while (1);
  }

  int connected = modem.joinOTAA(appEui, appKey);
}

void loop() {

  float voltageValue = analogRead(SoundSensorPin) / 1550.0 * VREF;
  float dbValue = voltageValue * 50.0;
  dbValue = constrain(dbValue, 30.0, 130.0);
  adjustSound(dbValue);

  float humidityValue = dht.readHumidity();
  humidityValue = constrain(humidityValue, 0.0, 100.0);
  adjustHumidity(humidityValue);

  float temperatureValue = dht.readTemperature();
  temperatureValue = constrain(temperatureValue, -40.0, 80.0);
  adjustTemperature(temperatureValue);

  adc_value = analogRead(ANALOG_IN_PIN);
  adc_voltage = (adc_value * ref_voltage) / 1024.0;
  in_voltage = adc_voltage * (R1 + R2) / R2;

  sds.wakeup();
  delay(2000);

  PmResult pm = sds.queryPm();
  if (pm.isOk()) {
    float pm25Value = pm.pm25;
    float pm10Value = pm.pm10;
    pm25Value = constrain(pm25Value, 0.0, 999.0);
    pm10Value = constrain(pm10Value, 0.0, 999.0);

      sds.sleep(); // Put SDS011 sensor to sleep

    // Sending Data via LoRa for PM values
    String dataToSend = String(dbValue, 0) + "," + String(humidityValue, 0) + "," +
                        String(temperatureValue, 1) + "," + String(in_voltage, 2) + "," + String(pm25Value, 1) +
                        "," + String(pm10Value, 1);

    //Serial.println(dataToSend);
    int err;
    modem.beginPacket();
    modem.print(dataToSend);
    err = modem.endPacket(true);
  }

  sds.sleep(); // Put SDS011 sensor to sleep

  // Triggers a 300000 ms sleep (5 minutes)
  LowPower.sleep(300000);
}
