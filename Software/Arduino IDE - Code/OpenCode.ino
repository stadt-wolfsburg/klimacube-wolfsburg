/*
 * SmartCityWolfsburg
 * Beispiel: KlimaCube - Wolfsburg
 * Referat 35 - Stadt Wolfsburg
 * 09-01-2024
 */

// Verwendete externe Bibliotheken
#include <SPI.h>
#include <DHT.h>
#include "SdsDustSensor.h"
#include <MKRWAN.h>
#include "ArduinoLowPower.h"

// Deklaration
#define DHTPIN 2
#define DHTTYPE DHT21
DHT dht(DHTPIN, DHTTYPE);

#define SoundSensorPin A3
#define VREF 5.0

#define AnalogVoltage A1
float adc_voltage = 0.0;
float in_voltage = 0.0;
float R1 = 30000.0;
float R2 = 7500.0;
float ref_voltage = 3.3;
int adc_value = 0;

SdsDustSensor sds(Serial1);

LoRaModem modem;

#include "arduino_secrets.h"
String appEui = SECRET_APP_EUI;
String appKey = SECRET_APP_KEY;

// SETUP
void setup() {
  // Initialisierung der Sensoren und des LoRa-Modems
  dht.begin();
  sds.begin();

  // Verbindung zum LoRa-Netzwerk herstellen
  int connected = modem.joinOTAA(appEui, appKey);
}

// LOOP
void loop() {
  // Lesen des Schallpegels und Umrechnung in Dezibel
  float voltageValue = analogRead(SoundSensorPin) / 1550.0 * VREF;
  float dbValue = voltageValue * 50.0;
  dbValue = constrain(dbValue, 30.0, 130.0);

  // Lesen der Luftfeuchtigkeit und Begrenzung des Wertebereichs
  float humidityValue = dht.readHumidity();
  humidityValue = constrain(humidityValue, 0.0, 100.0);

  // Lesen der Temperatur und Begrenzung des Wertebereichs
  float temperatureValue = dht.readTemperature();
  temperatureValue = constrain(temperatureValue, -40.0, 80.0);

  // Lesen der analogen Spannung und Berechnung der Eingangsspannung
  adc_value = analogRead(AnalogVoltage);
  adc_voltage = (adc_value * ref_voltage) / 1024.0;
  in_voltage = adc_voltage * (R1 + R2) / R2;

  // Aktivieren des SDS011-Sensors
  sds.wakeup();
  delay(2000);

  // Messung der Feinstaubwerte
  PmResult pm = sds.queryPm();
  if (pm.isOk()) {
    // Lesen der Feinstaubwerte und Begrenzung des Wertebereichs
    float pm25Value = pm.pm25;
    float pm10Value = pm.pm10;
    pm25Value = constrain(pm25Value, 0.0, 999.0);
    pm10Value = constrain(pm10Value, 0.0, 999.0);

    // SDS011-Sensor in den Ruhezustand versetzen
    sds.sleep();

    // Zusammenstellen der Daten für die LoRa-Übertragung
    String dataToSend = String(dbValue, 0) + "," + String(humidityValue, 0) + "," +
                        String(temperatureValue, 1) + "," + String(in_voltage, 2) + "," + String(pm25Value, 1) +
                        "," + String(pm10Value, 1);

    // Übertragung der Daten über LoRa
    int err;
    modem.beginPacket();
    modem.print(dataToSend);
    err = modem.endPacket(true);
  }

  // SDS011-Sensor in den Ruhezustand versetzen
  sds.sleep();

  // Wechsel in den Stromsparmodus für 5 Minuten
  LowPower.sleep(300000);
}
