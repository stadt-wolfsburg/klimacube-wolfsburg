# KlimaCube - Wolfsburg

![KlimaCube](/Image/KlimaCube_21.jpg){:width="700px" height="900px"}




## Inhaltsverzeichnis
1. [Einleitung](#einleitung)
2. [Benötigte Bauteile](#benötigte-bauteile)
3. [Benötigte Software](#benötigte-software)
4. [Bauteilanleitung in der Kurzversion](#bauteilanleitung-in-der-kurzversion)
5. [Testen](#testen)
6. [Abschluss](#abschluss)
7. [Sicherheitshinweise](#sicherheitshinweise)


## Einleitung
Willkommen beim KlimaCube-Projekt – unsere autarke Einheit, funktioniert wie eine standortunabhängige Wettermessstation. Über LoRaWAN sendet der KlimaCube zuverlässig und effizient erfasste Daten im 5-Minuten-Intervall. Mit einem modularen Aufbau und anpassungsfähiger Software ist der Zusammenbau und die Inbetriebnahme spielend einfach. Dank Akkubetrieb und der Option eines integrierbaren Solarmoduls bietet der KlimaCube eine langfristige Überwachung Ihrer Raumklima- und Umgebungsparameter.
Bei Interesse an individuellen Anpassungen unterstützen wir Sie gerne bei der Realisierung Ihrer Projektwünsche.

Die bereitgestellte Anleitung für das Projekt KlimaCube finden Sie im Ordner ["Bauanleitung"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/tree/main/Construction%20Manual?ref_type=heads)


## Benötigte Bauteile
Alle benötigten Bauteile für das Projekt finden sie im Ordner „Bauteile“ unter: 
["Bauteile"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/tree/main/Hardware/Bauteile?ref_type=heads)



Eine einfache Darstellung des gesamten Elektroschalplans finden Sie im Ordner „Schaltpläne“ unter:
["Schaltpläne"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/tree/main/Hardware/Schaltpl%C3%A4ne?ref_type=heads)

Die Grundplatten zur Bauteilbefestigung finden Sie im Ordner „Technische Zeichnungen“ unter:
["Technische Zeichnungen"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/tree/main/Hardware/Technische%20Zeichnungen?ref_type=heads)



## Benötigte Software
Benötigte Software: (Hinweis: Die Voraussetzung hierfür ist Verfügbarkeit der Software ["Arduino-IDE"](https://www.arduino.cc/en/software)
1.	Gehe zum Ordner „Arduino IDE – Code“ zu finden unter: ["Arduino IDE - Code"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/tree/main/Software/Arduino%20IDE%20-%20Code?ref_type=heads) und lade den Code ["OpenCode.ino"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/blob/main/Software/Arduino%20IDE%20-%20Code/OpenCode.ino?ref_type=heads) für den KlimaCube herunter.
2.	Öffne das Programm Arduino-IDE auf deinem Computer.
3.	Importiere den heruntergeladenen Code in die Arduino-IDE.
4.	Überprüfe die Pinbelegung im Code und passe sie gegebenenfalls an, wenn du andere Pins als in der Anleitung verwendest. Suche nach den Abschnitten im Code, die die Pin-Definitionen enthalten, und ändere sie entsprechend deiner Konfiguration.
5.	Verbinde deinen Arduino MKRWAN 1310 mit dem Computer und wähle den richtigen Port in Arduino-IDE aus.
6.	Lade den angepassten Code auf den Arduino MKRWAN 1310 hoch, indem du auf den Upload-Button in der Arduino-IDE klickst.
7.	Nach erfolgreichem Hochladen ist der KlimaCube einsatzbereit. Er misst nun kontinuierlich die anliegenden Parameter Lautstärke, Luftfeuchtigkeit, Temperatur, Luftqualität.

## Bauteilanleitung in der Kurzversion

Die vollständige Bauanleitung des KlimaCube finden Sie im unter ["Bauanleitung"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/blob/main/Construction%20Manual/Produktanleitung_eines_KlimaCubes.pdf?ref_type=heads)

Schritt 1: Vorbereitung der beiden Grundplatten

Nutze die technischen Zeichnungen im Ordner „technische Zeichnungen“, um die obere und unterer Grundplatte entsprechend anzufertigen. Anhand der Zeichnungen kannst du erkennen welche Maße zu wählen sind und wo genau die Bohrungen mit entsprechenden Durchmesser zu wählen sind. 

Schritt 2: Vorbereitung der unteren Grundplatte

Setze den Lärmsensor und den Luftqualitätssensor mit den Abstandshaltern in die vorgefertigten Bohrungen der unteren Trägerplatte ein. Befestige sie sicher. Bereite die Anschlussleitungen der Sensoren für die Verbindung mit der Lüsterklemme vor.

Schritt 3: Montagevorbereitung des Spannungssensors

Verwende eine M3x20mm Schraube, einen M3x6mm Abstandshalter und eine M3 Mutter, um den Spannungssensor vorzubereiten. Montiere ihn gemäß den Anweisungen.

Schritt 4: Vorbereitung des Mikrocontrollers, GSM-Antennen und Solar Power Managers

Kürze die überstehenden Pins des Mikrocontrollers und bohre die Durchgangslöcher für M3-Schrauben auf der Anschlussseite der GSM-Antennen. Montiere den Mikrocontroller mit zwei M3x15mm Schrauben und Muttern als Vorrichtung. Bereite den Solar Power Manager vor, indem du vier M3x6mm Schrauben, zwei M3x10mm Abstandhalter, zwei M3x10mm Abstandsbolzen mit Außengewinde und vier M3 Muttern verwendest.

Schritt 5: Montage der Komponenten auf der oberen Trägerplatte

Montiere den Mikrocontroller, den Spannungssensor, den Solar Power Manager sowie den Temperatur- und Luftfeuchtigkeitssensor auf der oberen Trägerplatte gemäß den Anweisungen im Schaltplan. Verdrahte die Kabel entsprechend.

Schritt 6: Endmontage und Verbindung

Verschraube die Unterseite und Oberseite mit vier M3x30mm Abstandshaltern und acht M3x10mm Edelstahl Senkkopfschrauben. Befestige die vorbereiteten Kabelverschraubungen an den entsprechenden Positionen und befestige den Mikrofonschaumstoff. Bereite das Solarmodul vor, führe die Durchgangsbohrung beim Antennenhalter durch, und bohre Halterungsbohrungen sowie eine Kabeldurchführungsbohrung im Gehäusedeckel. 
Schließe das Solarmodul an. Positioniere und verbinde beide Akkus. Schließe den Mikrocontroller mit einem USB-Kabel am Solar Power Manager an. Verbinde das GSM-Antennenanschlusskabel an der entsprechenden Stelle auf dem Mikrocontroller. Montiere die Befestigungsplatte mit den Schrauben und ggf. entsprechender Schraubensicherung an der dafür vorgesehenen Position. Zur Befestigung des KlimaCube können zwei Klemmschellen eingesetzt werden. 
Der KlimaCube ist nun einsatzbereit und kann über LoRaWAN an Dein bestehendes Netz verbunden werden.


## Testen
Platziere den KlimaCube an einem geeigneten Ort, um die bestmöglichen Ergebnisse erfassen zu können.
Beispiel einer möglichen Anwendung über TheThingsNetwork:
Um den KlimaCube in seiner Form testen zu können, kann über die Plattform TheThingsNetwork (TTN) eine Testanbindung erfolgen. Hierfür wird ein Account bei dem Betreiber (TTN) und vorzugsweise ein aktives LoRaWAN Gateway in ihrer Nähe benötigt. Wenn diese Anforderungen erfüllt sind, können sie Ihr Gerät erstmalig testen. 

Für die Implementierung auf TTN stehen Ihnen im Internet entsprechendes Infomaterial zur Verfügung zum Beispiel unter URL: (https://docs.arduino.cc/tutorials/mkr-wan-1310/the-things-network) (letzter Zugriff:16.01.2024, 09:48Uhr).

Um die Daten die an TTN übermittelt werden auswerten zu können wird ein PayLoad-Decoder benötigt. Das Beispiel für diesen PayLoad-Decoder finden Sie in der Datei ["PayLoad_TTN"](https://gitlab.opencode.de/OC000029088710/klimacube-wolfsburg/-/blob/main/Software/PayLoad_TTN?ref_type=heads). Dieser PayLoad muss in der Kategorie „Payload-Formatierer“ eingefügt werden. Hierbei handelt es sich um einen Benutzerdefinierten Javascript-Formatierer. 

Wenn alles geklappt hat sollten die Daten in der korrekten Form angezeigt werden siehe Abbildung:

![TTN-Beispiel](/Image/Ergebnis.PNG){:width="800px" height="400px"}


## Abschluss

Herzlichen Glückwunsch. Sie haben erfolgreich den KlimaCube in Betrieb genommen und getestet. Nun können Sie ihre Daten für Ihre nächsten Bearbeitungsprozesse und Anwendungen weiterverwenden. Viel Spaß beim Ausprobieren.

Als Tipp: In unserem Beispiel wurde der KlimaCube mittels Masthalterung an die Straßenbeleuchtungsanlage montiert. Diese Vorrichtung ist je nach Anwendungsfall nicht unbedingt erforderlich. Außerdem wenn einzelne Sensoren nicht relevant sind, können diese durch andere Sensortypen ersetzt werden. Hierfür müssen lediglich entsprechende Anpassungen vorgenommen werden.

![KlimaCube](/Image/rath..png)

## Sicherheitshinweise

**Die Stadt Wolfsburg übernimmt keine Haftung für das Produkt oder den Zusammenbau. Jegliche Risiken oder Schäden, die aus der Nutzung oder Montage entstehen, liegen in der Verantwortung des Benutzers oder Betreibers und nicht der Stadt Wolfsburg.**
